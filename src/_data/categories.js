const { default: axios } = require('axios');

module.exports = async () => {
  try {
    const res = await axios.get('http://localhost:1338/api/categories?populate=*');
    return res.data;
  } catch (error) {
    console.error(error);
  }
};