const { default: axios } = require('axios');

module.exports = async () => {
  try {
    const res = await axios.get('http://localhost:1338/api/works?populate=*');
    // console.log(res);
    return res.data;
  } catch (error) {
    console.error(error);
  }
};