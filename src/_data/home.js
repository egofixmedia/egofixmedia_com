const { default: axios } = require('axios');

module.exports = async () => {
  try {
    const res = await axios.get('http://localhost:1338/api/home?populate[Teaser][populate][0]=Image&populate[Mediaboxes][populate][1]=Image&populate[TextTop][populate][2]=*&populate[LandscapeMediabox][populate][3]=Image&populate[TextBottom][populate][4]=*');
    // console.log(res);
    return res.data;
  } catch (error) {
    console.error(error);
  }
};