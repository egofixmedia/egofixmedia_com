const { default: axios } = require('axios');

module.exports = async () => {
  try {
    const res = await axios.get('http://localhost:1338/api/sound?populate[ContentElement][populate][0]=Image');
    // console.log(res);
    return res.data;
  } catch (error) {
    console.error(error);
  }
};