---
title: Category
layout: base.njk
#pagination:
  #data: categories
  #size: 1
  #alias: category
#permalink: 'categories/{{ category.id }}/'
---

# {{ category.title }}

## works

{% for work in category.works %}

<li><a href="/works/{{ work.id }}/">{{ work.title }}</a></li>
{% endfor %}