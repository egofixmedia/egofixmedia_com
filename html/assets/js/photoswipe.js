(function() {


    // Init empty gallery array
    var container = [];
    var galleryParent = $('.gallery');
    var figure = $('.image');
    var imageLink = $('.js-photoSwipe');

    // Loop over gallery items and push it to the array
    imageLink.each(function() {
        var $link = $(this),

            item = {
                src: $link.attr('href'),
                //w: $link.data('width'),
                //h: $link.data('height'),
                title: $link.attr('title'),
                //title: $link.closest('.service-item__title').html,
                //title: 'Shalom Mwayi',
                // w: 'auto', // image width
                 h: 768 // image height
            };
        container.push(item);
    });

    // Define click event on gallery item
    imageLink.click(function(event) {

        // Prevent location change
        event.preventDefault();

        // Define object and gallery options
        var $pswp = $('.pswp')[0],
            options = {
                //index: $(this).parent(figure).index(),
                index: $(this).index(),
                bgOpacity: 0.85,
                showHideOpacity: true
            };

        // Initialize PhotoSwipe
        var gallery = new PhotoSwipe($pswp, PhotoSwipeUI_Default, container, options);
        gallery.init();
    });
})();