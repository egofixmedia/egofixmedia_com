/**
 * Namespace (if any)
 */

/* eslint-disable-next-line no-use-before-define */
var EGOFIX = EGOFIX || {};
EGOFIX.body = $("body");
EGOFIX.html = $("html");
EGOFIX.document = $(document);
EGOFIX.window = $(window);

(function() {
    var navContainer = $(".navigation");
    var navToggle = $(".nav-toggle");
    var navToggleBars = $(".nav-toggle .la-bars");
    var navToggleTimes = $(".nav-toggle .la-times");
    var nav = $(".nav");


    navToggle.click(function(event) {
        event.preventDefault();
        nav.toggleClass("hidden");
        navToggleBars.toggleClass("hidden");
        navToggleTimes.toggleClass("hidden");
    });

    // Prevent mobile navigation on screen width more than 768px
    EGOFIX.window
        .bind("resize", function() {
            if ($(this).width() < 768) {
                navToggle.removeClass("is-mobile");
                nav.addClass("md:block");
            }
        })
        .resize();
})();
/*
(function() {
    var swiper = new Swiper(".js-slider", {
        autoplay: {
            delay: 5000,
        },
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            dynamicBullets: true,
            clickable: true,
        },
        lazy: true,
        preloadImages: true,
    });
})();

(function() {
    var today = new Date().getHours();
    var dayTime = today >= 7 && today <= 18;

    if (dayTime) {
        EGOFIX.body.addClass("theme_light");
    } else {
        //EGOFIX.body.addClass("theme_dark");
    }
})();*/

/**
 * Add Header
 */
(function() {
    // Elements
    //var standardContent = $('.standard-content--iana');
    //var header = $('.header');
    //var headerHeight = header.height() + 20;
    //standardContent.css('padding-top', headerHeight);
})();

/**
 * Change header color after teaser
 */
/*
 * Back to Top(Scroll)
 */
(function() {
    var teaser = $(".teaser");
    var header = $(".header");
    var backTop = $('.back-top');

    var didScroll = function() {
        var headerHeight = header.height();

        if (EGOFIX.window.scrollTop() > headerHeight) {
            backTop.addClass('js-back-top--visible');
        } else {
            backTop.removeClass('js-back-top--visible');
        }
    };

    // Decide back to top button visibility on page load
    EGOFIX.document.ready(didScroll);

    // Decide back to top button visibility on scroll
    EGOFIX.window.on("scroll ready", didScroll);
})();
