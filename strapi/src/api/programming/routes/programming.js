'use strict';

/**
 * programming router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::programming.programming');
