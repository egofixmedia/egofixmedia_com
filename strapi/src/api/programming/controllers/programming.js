'use strict';

/**
 *  programming controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::programming.programming');
