module.exports = {
    content: ["./src/**/*.{html,njk,md,js}"],
    theme: {
        extend: {
            screens: {
                'ml': '960px',
                '2xl': '1700px',
            },
            colors: {
                limeish: '#d9ccaa',
                facebook: '#4267B2',
                instagram: '#C13584',
                twitter: '#1DA1F2',
                youtube: '#FF0000',
                linkedin: '#0077b5',
                whatsapp: '#25D366',
                design: '#CB7F07',
                sound: '#4a9396',
                programming: '#5f787b',

            },
            margin: {
                '2.5vw': '2.5vw',
            },
            padding: {
                '2.5vw': '2.5vw',
            },
            lineHeight: {
                'extra-loose': '2.5',
                '12': '3.5rem',
            },
            maxHeight: {
                '1400': '1400px',
            },
            fontFamily: {
                'sans': ['Poppins', 'sans-serif'],
            },
            aspectRatio: {
                '3/2': '3 / 2',
                '2/3': '2 / 2.81',
                '4/3': '4 / 3',
                '2/1': '2 / 1',
            },
            zIndex: {
                '1': '1',
            }
        },
    },
    plugins: [
         require('@tailwindcss/typography'),
    ],
}